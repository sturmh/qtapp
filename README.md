```
sudo apt-get update
sudo apt-get upgrade -y

sudo apt-get install python-qt4 pyqt4-dev-tools python-pip libzbar0 xinput
sudo pip install pyinstaller
sudo pip install pyzbar
sudo pip install Adafruit-ADS1x15
sudo pip install Adafruit-GPIO

```
 
If you are interfacing directly with the pi instead of through ssh and are not using a British keyboard, change the keyboard mapping and reboot
 
```
sudo sed -i -e '/XKBLAYOUT="gb"/ s/gb/us/' /etc/default/keyboard
reboot
```

Enable Camera by adding start_x=1 to /boot/config.txt

Rotate to portrait mode by adding 'display_rotate=1' to /boot/config.txt and rebooting

```
sudo vim /boot/config.txt
... add 'display_rotate=1'
... verify 'gpu_mem=128' exists (add if not)
sudo reboot

```










# TODO

```
enable i2c
```

sudo vi /etc/modules

```
add:
i2c-bcm2708
i2c-dev
```

sudo vi /boot/config.txt

```
uncomment or add:
dtparam=i2c1=on
dtparam=i2c_arm=on
```

sudo reboot



```
git clone https://sturmh@bitbucket.org/sturmh/qtapp
cd qtapp
./install.sh
```

At this point, there should be a shortcut on the desktop and the app should auto run on startup

