import sys
import signal
from PyQt4 import QtGui
from PyQt4 import QtCore
from ui__mainwindow import Ui_main_window
import logging
from Step1 import Step1
from Step2 import Step2
from Step3 import Step3
from Step4 import Step4
from Step5 import Step5
from Step6 import Step6
from Step7 import Step7
from Step8 import Step8
import time
logger = logging.getLogger()
from ui__loading import Ui_loading_widget
import os

def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS,
        # and places our data files in a folder relative to that temp
        # folder named as specified in the datas tuple in the spec file
        base_path = os.path.join(sys._MEIPASS, 'data')
    except Exception:
        # sys._MEIPASS is not defined, so use the original path
        base_path = '../data/'

    return os.path.join(base_path, relative_path)

class LoadingWidget(QtGui.QWidget, Ui_loading_widget):
    def __init__(self, parent = None):
        QtGui.QWidget.__init__(self, parent=parent)
        self.setupUi(self)
        self.loading_gif = QtGui.QMovie(resource_path('loading.gif'))
        self.loading_bar.setMovie(self.loading_gif)
    
    def set_left_text(self, text):
        self.left_label.setText(text)

    def set_right_text(self, text):
        self.right_label.setText(text)

    def start_loading_animation(self):
        self.loading_gif.start()

    def stop_loading_animation(self):
        self.loading_gif.stop()

class Window(QtGui.QMainWindow, Ui_main_window):
    BOTTOM_BAR_STATE_NONE = 0
    BOTTOM_BAR_STATE_STATUS = 1
    BOTTOM_BAR_STATE_LOADING = 2
    STATE_STRINGS = ['None', 'Status', 'Loading']
    button_signal = QtCore.pyqtSignal()

    def __init__(self, services, use_touchscreen=False, parent=None):
        QtGui.QMainWindow.__init__(self, parent=parent)
        self.setupUi(self)
        self.state = {}
        self.use_physical_button = not use_touchscreen
        self.stackedWidget = QtGui.QStackedWidget(self.centralwidget)
        self.stackedWidget.setEnabled(True)
        self.stackedWidget.addWidget(Step1(services, self))
        self.stackedWidget.addWidget(Step2(services, self))
        self.stackedWidget.addWidget(Step3(services, self))
        self.stackedWidget.addWidget(Step4(services, self))
        self.stackedWidget.addWidget(Step5(services, self))
        self.stackedWidget.addWidget(Step6(services, self))
        self.stackedWidget.addWidget(Step7(services, self))
        self.stackedWidget.addWidget(Step8(services, self))
        self.stackedWidget.setObjectName("stackedWidget")
        self.top.addWidget(self.stackedWidget, 0, 0, 1, 1)
        
        self.bottom_bar_state = self.BOTTOM_BAR_STATE_NONE
        self.enable_advance_keypress = False
        self.in_fail_state = False
        self.in_pass_state = False
        self.gpio = services['gpio']

        if self.use_physical_button == True and self.gpio == None:
            logger.error("Attempting to use external button with no gpio service - Forcing touchscreen")

        self.step_label = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(8)
        self.step_label.setFont(font)
        self.step_label.resize(100, 25)
        self.step_label.move(400, 775)
        self.step_label.show()

        if self.use_physical_button == True and self.gpio != None:
            self.status_label = QtGui.QLabel(self.bottom_widget)
            sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
            sizePolicy.setHeightForWidth(self.status_label.sizePolicy().hasHeightForWidth())
            self.status_label.setSizePolicy(sizePolicy)
            self.status_label.setObjectName("status_label")
            self.status_bar = self.status_label
            self.status_bar.setWordWrap(True)
            font = QtGui.QFont()
            font.setPointSize(18)
            self.status_label.setFont(font)
            self.button_signal.connect(self.__on_status_bar_tapped)
            self.gpio.add_external_button_callback(self.__on_physical_button_tap)
            self.enable_advance_keypress = True
        else:
            self.status_button = QtGui.QPushButton(self.bottom_widget)
            sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
            sizePolicy.setHeightForWidth(self.status_button.sizePolicy().hasHeightForWidth())
            self.status_button.setSizePolicy(sizePolicy)
            self.status_button.setObjectName("status_button")
            self.status_button.clicked.connect(self.__on_status_bar_tapped)
            # for some reasons the status_button area is extremely small..just relay events manually
            self.bottom_widget.installEventFilter(self)
            self.status_bar = self.status_button

        self.loading_widget = LoadingWidget(self.bottom_widget)
        self.bottom.addWidget(self.status_bar, 0, 0, 1, 1, QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
        self.bottom.addWidget(self.loading_widget, 0, 0, 1, 1)
        self.__set_current_widget(0) 
        self.last_tap = 0

    def eventFilter(self, obj, e):
        # for some reasons the status_button area is extremely small..just relay events manually
        if self.use_physical_button == False and obj == self.bottom_widget and e.type() == QtCore.QEvent.MouseButtonPress:
            if self.bottom_bar_state == self.BOTTOM_BAR_STATE_STATUS:
                self.status_button.animateClick()
                return True
        return False

    def keyPressEvent(self, e):
        # need to do fancier stuff to get other keys, for now dont care
        if e.key() == QtCore.Qt.Key_Escape:
	    e.accept()
	    self.close()

    def __update_step_number(self):
        self.step_label.setText("Step {0} of {1}".format(int(self.stackedWidget.currentIndex() + 1), int(self.stackedWidget.count())))

    def __on_physical_button_tap(self, channel):
        if time.time() - self.last_tap > 0.25:
            self.last_tap = time.time()
            self.button_signal.emit()

    def set_status_bar(self, message):
        self.__update_bottom_bar(self.BOTTOM_BAR_STATE_STATUS)
        self.status_bar.setText(message)

    def start_loading_bar(self, text):
        self.__update_bottom_bar(self.BOTTOM_BAR_STATE_LOADING)
        self.loading_widget.set_left_text(text)

    def __update_bottom_bar(self, desired_state):
        if desired_state == self.bottom_bar_state:
            return
        
        if desired_state == self.BOTTOM_BAR_STATE_STATUS:
            self.loading_widget.stop_loading_animation()
            self.loading_widget.set_left_text("")
            self.loading_widget.set_right_text("")
            self.loading_widget.hide()
            self.status_bar.show()
            self.bottom_bar_state = self.BOTTOM_BAR_STATE_STATUS
        else: # desired_state == self.BOTTOM_BAR_STATE_LOADING
            self.loading_widget.start_loading_animation()
            self.loading_widget.show()
            self.status_bar.setText("")
            self.status_bar.hide()
            self.bottom_bar_state = self.BOTTOM_BAR_STATE_LOADING

        logger.debug("bottom bar state set to {0}".format(self.STATE_STRINGS[self.bottom_bar_state]))

    def advance(self):
        widget_count = self.stackedWidget.count() - 1
        current_widget_index = self.stackedWidget.currentIndex()
        next_widget_index = current_widget_index + 1
        if next_widget_index > widget_count:
            logger.debug("advance is looping around, reseting state {0}".format(self.state))
            self.state = {}
            next_widget_index = 0

        logging.debug("advance step from {0} to {1}".format(current_widget_index, next_widget_index))
        self.__current_widget().on_exit(self.state)
        logger.debug("state after exit:{0}".format(self.state))
        self.__set_current_widget(next_widget_index)

    def enter_fail_state(self, message):
        self.in_fail_state = True
        logger.debug("window in fail state: in_fail_state:{0} in_pass_state:{1}".format(self.in_fail_state, self.in_pass_state))
        self.set_status_bar("{0}".format(message))

    def enter_pass_state(self, message):
        self.in_pass_state = True
        logger.debug("window in pass state in_fail_state:{0} in_pass_state:{1}".format(self.in_fail_state, self.in_pass_state))
        self.set_status_bar("{0}".format(message))

    def reset(self):
        self.in_fail_state = False
        self.in_pass_state = False
        self.state = {}
        self.__current_widget().reset()
        self.__set_current_widget(0)
        logger.debug("window reset")

    def __current_widget(self):
        return self.stackedWidget.currentWidget()

    def __on_status_bar_tapped(self):
        logger.debug("status_bar_tapped -> in_fail_state:{0} in_pass_state:{1}".format(self.in_fail_state, self.in_pass_state))
        if self.in_fail_state:
            self.in_fail_state = False
            self.reset()
            return
        if self.in_pass_state:
            self.in_pass_state = False
            self.advance()
            return
        self.__current_widget().run_step()
    
    def __set_current_widget(self, idx):
        self.stackedWidget.setCurrentIndex(idx)
        self.__update_step_number()
        logger.debug("state before enter:{0}".format(self.state))
        self.__current_widget().on_enter(self.state)
