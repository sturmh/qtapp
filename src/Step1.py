import sys
import signal
from PyQt4 import QtGui
from PyQt4 import QtCore
from ui__step1 import Ui_step_1
from ButtonListWidgetItem import ButtonListWidgetItem
import logging
logger = logging.getLogger(__name__)

class Step1(QtGui.QWidget, Ui_step_1):
    buttons = ["VBAT_CURRENT", "VDD", "VAA", "VLED", "AO_DETECT"]

    def __init__(self, services, window, params = {}, parent = None):
        QtGui.QWidget.__init__(self, parent=parent)
        self.setupUi(self)
        self.window = window
        self.services = services
        self.dac = self.services['dac']
        self.mcp4728 = self.services['mcp4728']
        self.mcp47feb22 = self.services['mcp47feb22']
        self.items = {}
        for indx, button in enumerate(self.buttons):
            itemWidget = ButtonListWidgetItem(button)
            itemWidget.setState(0)
            self.verticalLayout.addWidget(itemWidget, 0, QtCore.Qt.AlignTop);
            self.items[button] = itemWidget
        
    # called whenever this widget is transitioning to being the current widget
    def on_enter(self, state):
        logger.debug("entering")
        self.reset()

        if self.dac == None or self.mcp4728 == None or self.mcp47feb22 == None:
            if self.dac == None:
                self.window.enter_fail_state("DAC not connected")
            elif self.mcp4728 == None:
                self.window.enter_fail_state("mcp4728 not connected")
            elif self.mcp47feb22 == None:
                self.window.enter_fail_state("mcp47feb22 not connected")
        else:
            #init AO_P to 0
            self.mcp4728.set_value(1, 0)
            #init IG_P/N to mid-ref
            self.mcp47feb22.set_voltage(0, 0.5)
            self.mcp47feb22.set_voltage(1, 0.5)
            self.window.set_status_bar("Press Yellow button to start test")

    # called whenever this widget is transitioning from being the current widget
    def on_exit(self, state):
        logger.debug("exiting")
        self.reset()

    # called whenever this widget is maintain focus as current widget but needs to reset state
    def reset(self):
        for key, button in self.items.iteritems():
            button.setState(0)

    def run_step(self):
        has_failure = False
        for key, button in self.items.iteritems():
            result = self.dac.validate(key)
            logger.debug("{0} validation retunred {1}".format(key, result))
            button.setState(result)
            if result != 2:
                has_failure = True

        if has_failure:
            self.window.enter_fail_state("Validation failed")
        else:
            self.window.enter_pass_state("Press Yellow button to continue")
