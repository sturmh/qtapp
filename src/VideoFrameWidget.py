from PyQt4 import QtGui
from PyQt4 import QtCore

class VideoFrameWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(VideoFrameWidget, self).__init__(parent)
        self.image = None
        self.update()

    def setImage(self, image):
        self.image = image
        if image:
            sz = image.size()
            self.setMinimumSize(sz)
        self.update()

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        if self.image:
            qp.drawImage(QtCore.QPoint(0, 0), self.image)
        qp.end()