import sys
import signal
from PyQt4 import QtGui
from PyQt4 import QtCore
from ui__step1 import Ui_step_1
from ButtonListWidgetItem import ButtonListWidgetItem
import logging
from threading import Thread
import time
logger = logging.getLogger(__name__)


class Step8(QtGui.QWidget, Ui_step_1):
    buttons = ["M.Const"]
    programming_complete_signal = QtCore.pyqtSignal(bool)

    def __init__(self, services, window, params = {}, parent = None):
        QtGui.QWidget.__init__(self, parent=parent)
        self.setupUi(self)
        self.window = window
        self.services = services
        self.serial0 = self.services['serial0']
        self.mfg_string = None
        self.items = {}
        for indx, button in enumerate(self.buttons):
            itemWidget = ButtonListWidgetItem(button)
            itemWidget.setState(0)
            self.verticalLayout.addWidget(itemWidget, 0, QtCore.Qt.AlignTop);
            self.items[button] = itemWidget
        self.programming_complete_signal.connect(self.__on_programming_completed)

    # called whenever this widget is transitioning to being the current widget
    def on_enter(self, state):
        logger.debug("entering")
        if self.serial0 == None:
            logger.error("No serial0 connected")
            self.window.enter_fail_state("Serial0 not connected")
            return

        if 'mfg_string' not in state:
            logger.error("No mfg string in state")
            self.window.enter_fail_state("Failed to get mfg string")
            return

        self.reset()
        self.mfg_string = state['mfg_string']
        self.window.start_loading_bar("Programming")
        self.programming_thread = Thread(target=self.__do_programming, args=())
        self.programming_thread.start()

    # called whenever this widget is transitioning from being the current widget
    def on_exit(self, state):
        logger.debug("exiting")
        if self.programming_thread != None:
            self.programming_thread.join()

        # reset mfg string
        state['mfg_string'] = None
        self.reset()

    # called whenever this widget is maintain focus as current widget but needs to reset state
    def reset(self):
        for key, button in self.items.iteritems():
            button.setState(0)

        self.mfg_string = None
        self.programming_thread = None

    def run_step(self):
        logger.debug("running")
        self.window.advance()
    
    def __do_programming(self):
        did_save = False
        did_restart = False
        
        self.serial0.flush()
        bytes_to_flush = self.serial0.inWaiting()
        if bytes_to_flush > 0:
            data = self.serial0.read(bytes_to_flush)
            logger.debug("flushing uart")
            logger.debug("{0}".format(data))
        
        uart_command = "mfg save -f 0101{0}0000\n".format(self.mfg_string)
        logger.debug("writing uart command: '{0}'".format(uart_command))
        self.serial0.write(uart_command)
        
        did_save = False
        start = time.time()
        success_str = "mfg: saving ... [ PASS ]"
        fail_str = "mfg: saving ... [ FAIL ]"
        logger.debug("Waiting for confirmation string of '{0}'".format(success_str))
        while (time.time() - start < 10):
            line = self.serial0.readline()
            logger.debug("uart_output: {0}".format(line.rstrip()))
            if success_str in line:
                logger.debug("found '{0}' in '{1}'".format(success_str, line.rstrip()))
                did_save = True
                break
            elif fail_str in line:
                logger.debug("found '{0}' in '{1}'".format(fail_str, line.rstrip()))
                did_save = False
                break
            
        logger.debug("mfg save command {0}".format("PASSED" if did_save else "FAILED"))

        if did_save:
            logger.debug("Waiting for restart")
            start = time.time()
            restart_confirm_str = "Zurich"
            while (time.time() - start < 10):
                line = self.serial0.readline()
                logger.debug("uart_output: {0}".format(line.rstrip()))
                if restart_confirm_str in line:
                    did_restart = True
                    break

            logger.debug("{0}".format("Restart detected" if did_restart else "Timed out waiting for restart"))
        
        self.programming_complete_signal.emit(did_save and did_restart)

    def __on_programming_completed(self, success):
        logger.debug("__on_programming_complete({0})".format(success))
        for key, button in self.items.iteritems():
            button.setState(2 if success else 1)

        if success:
            self.window.enter_pass_state("Complete - Press button to reset")
        else:
            self.window.enter_fail_state("Failed to program")
