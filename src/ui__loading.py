# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'loading.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_loading_widget(object):
    def setupUi(self, loading_widget):
        loading_widget.setObjectName(_fromUtf8("loading_widget"))
        loading_widget.resize(400, 300)
        self.horizontalLayout = QtGui.QHBoxLayout(loading_widget)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.left_label = QtGui.QLabel(loading_widget)
        self.left_label.setText(_fromUtf8(""))
        self.left_label.setObjectName(_fromUtf8("left_label"))
        self.horizontalLayout.addWidget(self.left_label, 0, QtCore.Qt.AlignRight)
        self.loading_bar = QtGui.QLabel(loading_widget)
        self.loading_bar.setMinimumSize(QtCore.QSize(128, 15))
        self.loading_bar.setMaximumSize(QtCore.QSize(128, 15))
        self.loading_bar.setText(_fromUtf8(""))
        self.loading_bar.setObjectName(_fromUtf8("loading_bar"))
        self.horizontalLayout.addWidget(self.loading_bar, 0, QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.right_label = QtGui.QLabel(loading_widget)
        self.right_label.setText(_fromUtf8(""))
        self.right_label.setObjectName(_fromUtf8("right_label"))
        self.horizontalLayout.addWidget(self.right_label)

        self.retranslateUi(loading_widget)
        QtCore.QMetaObject.connectSlotsByName(loading_widget)

    def retranslateUi(self, loading_widget):
        loading_widget.setWindowTitle(_translate("loading_widget", "Form", None))

