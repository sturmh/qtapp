import time
import logging
logger = logging.getLogger(__name__)

IG_CURRENT_V_PER_A=0.02*100     # 20mOhm sense, gain 100 amp
AO_CURRENT_V_PER_A=1.0*100      # 1Ohm sense, gain 100 amp
IG_CURRENT_OFFSET=0
AO_CURRENT_OFFSET=0
Vnom = {
        "VDD" : 2.820,
        "VCC" : 3.245,
        "VAA" : 2.992
        }

# Ranges as specified by the Functional Tester Protocol 20150816
VDD_RANGE = [2.72, 2.92]
VAA_RANGE = [2.89, 3.09]
VLED_RANGE = [2.8, 3.6]

# Functional tester protocol calls for AO_DETECT < 1.4V when AO_5V
# present and > 1.4V if not.
# However, microcontroller logic levels are actually 0.2*Vdd and 0.8*Vdd
AO_DETECT_LOGIC_LEVELS = [0.2*2.72, 0.8*2.92]

## Outer boundaries based on all component tolerances (148Rev03)
#VDD_RANGE = [2.680, 2.941]
#VAA_RANGE = [2.890, 3.097]
VCC_RANGE = [3.072, 3.396]
#VLED_RANGE = [2.000, 3.600] #TODO: beware of PWM effect
VCC_VAA_MIN = 0.05

V_INACTIVE = 0.1

AO_CURRENT_RANGE = [0.003, 0.012]
VBAT_CURRENT_RANGE = [0.050, 0.450]

class DAC:
    def __init__(self, is_ADC3_16b):
        from Adafruit_ADS1x15 import ADS1015
        from Adafruit_ADS1x15 import ADS1115
        self.is_ADC3_16b = is_ADC3_16b
        self.adc1 = ADS1015(address=0x48)
        self.adc2 = ADS1015(address=0x49)
        self.adc1._data_rate_config(128)
        self.adc2._data_rate_config(128)
        if self.is_ADC3_16b:
            self.adc3 = ADS1115(address=0x4b)
            self.adc3._data_rate_config(8)
        else:
            self.adc3 = ADS1015(address=0x4b)
            self.adc3._data_rate_config(128)

        self.function_map = {"VDD": self.validate_VDD, 
            "VCC": self.validate_VCC,
            "VAA": self.validate_VAA,
            "VLED": self.validate_VLED,
            "VBAT_CURRENT": self.validate_VBAT_current,
            "AO_CURRENT": self.validate_AO_current,
            "AO_DETECT": self.validate_AO_DETECT
            }
        

    # Convert ADS1015 reading to voltage
    def __i12_to_V(self, raw, gain):
        vfs = 6.144;
        if(gain>=1):
            vfs = 4.096/gain
        lsb = vfs*2/(1 << 12)
        return lsb*raw

    # Convert ADS1115 reading to voltage
    def __i16_to_V(self, raw, gain):
        vfs = 6.144;
        if(gain>=1):
            vfs = 4.096/gain
        lsb = vfs*2/(1 << 16)
        return lsb*raw

    def __iX_to_V(self, raw, gain):
        v = 0
        if(self.is_ADC3_16b):
            v = self.__i16_to_V(raw, gain)
        else:
            v = self.__i12_to_V(raw, gain)
        return v

    def read_VDD(self, gain=1):
        # More accurate to read relative to AGND
        #raw = self.adc1.read_adc(0, gain)
        raw = self.adc1.read_adc_difference(1, gain) # 0 minus 3
        return self.__i12_to_V(raw, gain)

    # Actually measures VLED/2
    def read_VLED(self, gain=2):
        # More accurate to read relative to AGND
        #raw = self.adc1.read_adc(1, gain)
        raw = self.adc1.read_adc_difference(2, gain) # 1 minus 3
        return 2.0*self.__i12_to_V(raw, gain)

    def read_VAA(self, gain=1):
        # More accurate to read relative to AGND
        #raw = self.adc1.read_adc(2, gain)
        raw = self.adc1.read_adc_difference(3, gain) # 2 minus 3
        return self.__i12_to_V(raw, gain)

    # PCBA ground is raised relative to Fixture board
    # due to cable IR drop
    def read_AGND(self, gain=16):
        raw = self.adc1.read_adc(3, gain)
        #LATER: use averaging since this is noisy due to PWM current
        return self.__i12_to_V(raw, gain)

    def read_AO_current(self, gain=2):
        raw = self.adc2.read_adc(0, gain)
        raw = raw - AO_CURRENT_OFFSET
        if (raw < 0): raw = 0
        return self.__i12_to_V(raw, gain)/AO_CURRENT_V_PER_A

    def read_VBAT_current(self, gain=2):
        raw = self.adc2.read_adc(1, gain)
        raw = raw - IG_CURRENT_OFFSET
        if (raw < 0): raw = 0
        return self.__i12_to_V(raw, gain)/IG_CURRENT_V_PER_A

    #WIP
    def read_VBAT_current_avg(self):
        isum = self.read_VBAT_current()
        ilo = isum
        ihi = isum
        for i in range(30):
            time.sleep(0.008333)
            icur = self.read_VBAT_current()
            if(icur < ilo):
                ilo = icur
            if(icur > ihi):
                ihi = icur
            isum = isum + icur
            iavg = isum / 31
            pct = (iavg-ilo)/(ihi-ilo)
        return [ilo, iavg, ihi, pct]

    def read_VCC(self, gain=2):
        # actually measureing Vcc/2
        raw = self.adc2.read_adc(2, gain)
        v = 2.0*self.__i12_to_V(raw, gain)
        v = v - self.read_AGND() # compensate for ground resistance
        return v

    def read_AO_DETECT(self, gain=1):
        # range: 0-VDD
        raw = self.adc2.read_adc(3, gain)
        v = self.__i12_to_V(raw, gain)
        v = v - self.read_AGND() # compensate for ground resistance
        return v

    def read_IG_P(self, gain=2):
        logger.debug("read_IG_P Assumes IG_P soldered to TP3")
        raw = self.adc3.read_adc(2, gain)
        v = self.__iX_to_V(raw, gain)
        v = v - self.read_AGND() # compensate for ground resistance
        return v

    def read_IG_N2(self, gain=2):
        logger.debug("read_IG_N2 Assumes IG_N soldered to TP4")
        raw = self.adc3.read_adc(3, gain)
        v = self.__iX_to_V(raw, gain)
        v = v - self.read_AGND() # compensate for ground resistance
        return v

    def read_IG_PN(self, gain=16):
        logger.debug("read_IG_PN Assumes IG_P soldered to TP3 and IG_N to TP4")
        raw = self.adc3.read_adc_difference(3, gain)
        v = self.__iX_to_V(raw, gain)
        v = v - self.read_AGND() # compensate for ground resistance
        return v

    def read_IG_N(self, gain=2):
        raw = self.adc3.read_adc(0, gain)
        v = self.__iX_to_V(raw, gain)
        return v

    def read_IG_D(self, gain=2):
        raw = self.adc3.read_adc(1, gain)
        v = self.__iX_to_V(raw, gain)
        return v

    def read_IG_N_agnd(self):
        v = self.read_IG_N()
        v = v - self.read_AGND() # compensate for ground resistance
        return v

    def read_IG_D_agnd(self):
        v = self.read_IG_D()
        v = v - self.read_AGND() # compensate for ground resistance
        return v

    #TODO: are thees relative to GND or AGND
    def read_IG_N_ratio(self):
        return self.read_IG_N()/self.read_VAA()

    #TODO: are thees relative to GND or AGND
    def read_IG_D_ratio(self):
        return self.read_IG_D()/self.read_VAA()

    def read_IG_T(self, gain=16):
        # 0 minus 1
        raw = self.adc3.read_adc_difference(0, gain)
        return self.__iX_to_V(raw, gain)

    def read_IG_T_ratio(self):
        return self.read_IG_T()/self.read_VAA()

    # 0 = inactive
    # 1 = off
    # 2 = on
    def validate(self, string):
        if string in self.function_map:
            return self.function_map[string]()
        else:
            logger.error("attempting to validate unknown key {0}".format(string))
            return 0

    def validate_VDD(self):
        val = self.read_VDD()
        if (val < V_INACTIVE):
            return 0
        if ((val < VDD_RANGE[0]) | (val > VDD_RANGE[1])):
            return 1
        return 2

    def validate_VAA(self):
        val = self.read_VAA()
        if (val < V_INACTIVE):
            return 0
        if ((val < VAA_RANGE[0]) | (val > VAA_RANGE[1])):
            return 1
        return 2

    def validate_VLED(self):
        val = self.read_VLED()
        if (val < V_INACTIVE):
            return 0
        if ((val < VLED_RANGE[0]) | (val > VLED_RANGE[1])):
            return 1
        return 2

    def validate_VCC(self):
        val = self.read_VCC()
        if (val < V_INACTIVE):
            return 0
        if ((val < VCC_RANGE[0]) | (val > VCC_RANGE[1])):
            return 1
        return 2

    def validate_VCC_vs_VAA(self):
        vcc = self.read_VCC()
        vaa = self.read_VAA()
        if (vcc < vaa):
            return 0
        if ((vcc-vaa) < VCC_VAA_MIN):
            return 1
        return 2

    # currently can't turn of AO_5V so can only validate low level
    def validate_AO_DETECT(self):
        val = self.read_AO_DETECT()
        if (val < 0.005):
            return 0
        #if (val > 0.2*Vnom["VDD"]):
        #    return 1
        if (val > AO_DETECT_LOGIC_LEVELS[0]):    # above logic-low level
            return 1
        return 2

    def validate_AO_current(self):
        val = self.read_AO_current()
        if ((val < AO_CURRENT_RANGE[0]) | (val > AO_CURRENT_RANGE[1])):
            return 1
        return 2

    def validate_VBAT_current(self):
        val = self.read_VBAT_current()
        if ((val < VBAT_CURRENT_RANGE[0]) | (val > VBAT_CURRENT_RANGE[1])):
            return 1
        return 2
