#!/usr/bin/python


class MCP47FEB22:
    i2c = None
        
    # Registers
    __REG_WRITEALLDAC   = 0x50
    __REG_WRITEPROMCH0  = 0x58
    __REG_WRITEPROMCH1  = 0x5A
    __REG_WRITEPROMCH2  = 0x5C
    __REG_WRITEPROMCH3  = 0x5E

    # Constructor
    def __init__(self, address=0x61, busnum=1, debug=False):
        from Adafruit_GPIO.I2C import Device as Adafruit_I2C
        self.i2c = Adafruit_I2C(address, busnum)
        self.address = address
        self.debug = debug

    def set_value(self, channel, n):
        command = 0x00 | (channel << 3) & 0xF8 
        n = min(max(0, n), 4095)
        bytes = [(n >> 8) & 0xFF, n & 0xFF]
        print command, bytes
        self.i2c.writeList(command, bytes)

    # default reference is buffered-Vref
    def set_voltage(self, channel, rel):
        n = int(rel*4096 + 0.5)
        self.set_value(channel, n)

