import sys
import signal
from PyQt4 import QtGui
from PyQt4 import QtCore
from ui__step1 import Ui_step_1
import logging
from VideoFrameWidget import VideoFrameWidget
import math
import Queue
import threading
import pyzbar.pyzbar as pyzbar
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
logger = logging.getLogger(__name__)

class Step7(QtGui.QWidget, Ui_step_1):

    def __init__(self, services, window, params = {}, parent = None):
        QtGui.QWidget.__init__(self, parent=parent)
        self.setupUi(self)
        self.window = window
        self.services = services
        self.camera = self.services['camera']
        self.window_width = 480
        self.window_height = 360
        self.video_frame = VideoFrameWidget()
        self.verticalLayout.addWidget(self.video_frame)
        self.program_thread = None
        self.running = False;
        self.did_pass = False
        self.mfg_string = None

    def stop_video(self):
        if self.running == False:
            return

        self.running = False 
        self.video_frame_updater.stop()
        self.video_frame.setImage(None)
        self.decode_result_handler.stop()
        self.capture_thread.join()
        self.decode_thread.join()
        self.display_queue = None
        self.decode_queue = None
        self.decoded_result = None
        logger.debug("Stopping video")

    def start_video(self):
        if self.camera == None:
            logger.warning("Can't start video because camera not connected")
            return

        if self.running == True:
            return

        logger.debug("Starting video")
        
        self.running = True
        self.display_queue = Queue.Queue(32)
        self.decode_queue = Queue.Queue(32)
        self.video_frame_updater = QtCore.QTimer(self)
        self.video_frame_updater.timeout.connect(self.__update_frame)
        self.video_frame_updater.start(41) #millis
        self.decode_result_handler = QtCore.QTimer(self)
        self.decode_result_handler.timeout.connect(self.__handle_decode_result)
        self.decode_result_handler.start(500) #millis
        self.capture_thread = threading.Thread(target=self.__grab, args = ())
        self.decode_thread = threading.Thread(target=self.__decode, args = ())
        self.capture_thread.start()
        self.decode_thread.start()
        self.decoded_result = None

    def __grab(self):
        count = 0 
        rawCapture = PiRGBArray(self.camera)
        for frame in self.camera.capture_continuous(rawCapture, format="bgr", use_video_port=False):
            if self.running == False:
                break

            count = count + 1

            image = frame.array
            frame = {'img':image}

            try:
                self.display_queue.put_nowait(frame)
            except Queue.Full:
                logger.warning("display queue is full on frame - flushing")
                with self.display_queue.mutex:
                    self.display_queue.queue.clear()

            # only try to decode 1 in 2 frames
            if count % 2 == 0:
                try:
                    self.decode_queue.put_nowait(frame)
                except Queue.Full:
                    logger.warning("decode queue is full on frame - flushing")
                    with self.decode_queue.mutex:
                        self.decode_queue.queue.clear()
                count = 0
            
            rawCapture.truncate()
            rawCapture.seek(0)

    def __decode(self):
        # wait 2 seconds before we start decoding any frames
        time.sleep(2)

        while self.running:
            try:
                # block and timeout after 1 second
                frame = self.decode_queue.get(True, 1)
            except Queue.Empty:
                continue


            logger.debug("Attempting decode {0}".format(self.decode_queue.qsize()))
            img = frame["img"]
            
            results = pyzbar.decode(img, symbols=[pyzbar.ZBarSymbol.QRCODE])
            if results:
                logger.debug("Decoded frame:{0}".format(results))
                self.decoded_result = str(results[0][0])


    def __update_frame(self):
        if self.display_queue.empty() == False:
            frame = self.display_queue.get()
            img = frame["img"]
            height, width, bpc = img.shape
            bpl = bpc * width
            image = QtGui.QImage(img.tostring(), width, height, bpl, QtGui.QImage.Format_RGB888)
            scaled_image = image.scaled(self.window_width, self.window_height, QtCore.Qt.KeepAspectRatio)
            matrix = QtGui.QMatrix()
            matrix.rotate(180)
            scaled_image = scaled_image.transformed(matrix)
            self.video_frame.setImage(scaled_image)
            logger.debug("Frame {0}".format(self.display_queue.qsize()))

    def __handle_decode_result(self):
        if self.decoded_result == None:
            return

        result = self.decoded_result
        logger.debug("Obtained decoded result from camera:{0}".format(result))
        self.stop_video();

        splits = result.split(",")
        if len(splits) == 0:
            logger.error("Failed to extract data from {0}".format(result))
            self.window.enter_fail_state()
            return

        self.mfg_string = splits[0][-32:]
        self.window.advance()

    # called whenever this widget is transitioning to being the current widget
    def on_enter(self, state):
        logger.debug("entering")
        if self.camera == None:
            self.window.set_status_bar("Failure: Camera not connected")
            return

        self.reset()
        self.start_video()
        self.window.start_loading_bar("Testing")

    # called whenever this widget is transitioning from being the current widget
    def on_exit(self, state):
        logger.debug("exiting")
        if self.mfg_string != None:
            state['mfg_string'] = self.mfg_string
        self.reset()

    # called whenever this widget is maintain focus as current widget but needs to reset state
    def reset(self):
        self.stop_video()
        self.mfg_string = None

    def run_step(self):
        if self.camera == None:
            logger.debug("No camera connect, looping back to start")
            self.window.reset()
        self.window.reset()
