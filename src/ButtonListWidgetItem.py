from PyQt4 import QtGui
from PyQt4 import QtCore
from ui__button_listwidget_item import Ui_button_listwidget_item

class ButtonListWidgetItem(QtGui.QWidget):
    INACTIVE = 0
    OFF = 1
    ON = 2

    def __init__(self, text, parent = None):
        super(ButtonListWidgetItem, self).__init__(parent)        
        self.ui = Ui_button_listwidget_item()
        self.ui.setupUi(self)        
        self.ui.label.setText(text) 
        stripped_test = text.replace(".", "_")
        self.ui.label.setObjectName(self.ui.label.objectName() + "_" + stripped_test)       
        self.ui.status_led.setObjectName(self.ui.status_led.objectName() + "_" + stripped_test)
        self.setState(ButtonListWidgetItem.INACTIVE)           

    def setState(self, state):
        assert state == 0 or state == 1 or state == 2
        self.state = state

        if state == ButtonListWidgetItem.INACTIVE:
            width = self.ui.status_led.size().width()
            s = '''                
                QLabel#%s{ 
                    background-color:rgb(200, 200, 200); 
                    border-style:outset;                     
                    border-radius:%dpx;
                }
                ''' % (self.ui.status_led.objectName(), (width / 2))

            self.ui.status_led.setStyleSheet(s)
            self.ui.label.setStyleSheet('''color:rgb(200, 200, 200)''')
        elif state == ButtonListWidgetItem.OFF:
            width = self.ui.status_led.size().width()
            s = '''                
                QLabel#%s{ 
                    background-color:red; 
                    border-style:outset;                     
                    border-radius:%dpx;
                }
                ''' % (self.ui.status_led.objectName(), (width / 2))

            self.ui.status_led.setStyleSheet(s)
            self.ui.label.setStyleSheet('''color:black''')
        elif state == ButtonListWidgetItem.ON:
            self.ui.label.setStyleSheet('''color:black''')
            width = self.ui.status_led.size().width()
            s = '''                
                QLabel#%s{ 
                    background-color:green; 
                    border-style:outset;                     
                    border-radius:%dpx;
                }
                ''' % (self.ui.status_led.objectName(), (width / 2))

            self.ui.status_led.setStyleSheet(s)
