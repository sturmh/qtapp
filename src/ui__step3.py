# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'step3.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_step_3(object):
    def setupUi(self, step_3):
        step_3.setObjectName(_fromUtf8("step_3"))
        step_3.resize(400, 300)
        self.verticalLayout = QtGui.QVBoxLayout(step_3)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.image = QtGui.QLabel(step_3)
        self.image.setMinimumSize(QtCore.QSize(273, 210))
        self.image.setMaximumSize(QtCore.QSize(273, 210))
        self.image.setText(_fromUtf8(""))
        self.image.setObjectName(_fromUtf8("image"))
        self.verticalLayout.addWidget(self.image, 0, QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
        self.description = QtGui.QLabel(step_3)
        self.description.setText(_fromUtf8(""))
        self.description.setObjectName(_fromUtf8("description"))
        self.verticalLayout.addWidget(self.description, 0, QtCore.Qt.AlignHCenter|QtCore.Qt.AlignTop)

        self.retranslateUi(step_3)
        QtCore.QMetaObject.connectSlotsByName(step_3)

    def retranslateUi(self, step_3):
        step_3.setWindowTitle(_translate("step_3", "Form", None))

