# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'step2.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_step_2(object):
    def setupUi(self, step_2):
        step_2.setObjectName(_fromUtf8("step_2"))
        step_2.resize(480, 800)
        step_2.setMaximumSize(QtCore.QSize(480, 800))
        self.verticalLayout = QtGui.QVBoxLayout(step_2)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.center_image = QtGui.QLabel(step_2)
        self.center_image.setMinimumSize(QtCore.QSize(115, 247))
        self.center_image.setMaximumSize(QtCore.QSize(115, 250))
        self.center_image.setText(_fromUtf8(""))
        self.center_image.setObjectName(_fromUtf8("center_image"))
        self.horizontalLayout.addWidget(self.center_image, 0, QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.description = QtGui.QLabel(step_2)
        self.description.setObjectName(_fromUtf8("description"))
        self.verticalLayout.addWidget(self.description, 0, QtCore.Qt.AlignHCenter|QtCore.Qt.AlignTop)

        self.retranslateUi(step_2)
        QtCore.QMetaObject.connectSlotsByName(step_2)

    def retranslateUi(self, step_2):
        step_2.setWindowTitle(_translate("step_2", "Form", None))
        self.description.setText(_translate("step_2", "TextLabel", None))

