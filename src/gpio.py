import time

pins={
        "pushbutton"        : 12,
        "eeprom_en"         : 25,
        "sel_prog1"         : 23,
        "sel_prog2"         : 24,
        "vbat_disable"      : 18,
        "disable_backlight" : 22,
        "rdy_dac1"          : 17,
        "dac1_ld_bar"       : 27,
        "lat_dac2"          : 21,
        "rdy_adc1"          : 16,
        "rdy_adc2"          : 13,
        "rdy_adc3"          : 19,
        "rsv1"              : 4,
        "rsv2"              : 5,
        "rsv3"              : 6,
        "rsv4"              : 26,
        "external_button"   : 20
    }

class GPIO:
    def __init__(self):
        import Adafruit_GPIO as GPIO
        import Adafruit_GPIO.PWM as PWM
        self.gpio = GPIO.get_platform_gpio()
        self.pwm = PWM.get_platform_pwm()

        self.gpio.setup(pins["pushbutton"], GPIO.OUT)
        self.gpio.setup(pins["eeprom_en"], GPIO.OUT)
        self.gpio.setup(pins["sel_prog1"], GPIO.OUT)
        self.gpio.setup(pins["sel_prog2"], GPIO.OUT)
        self.gpio.setup(pins["vbat_disable"], GPIO.OUT)
        self.gpio.setup(pins["disable_backlight"], GPIO.OUT)

        self.gpio.setup(pins["rdy_dac1"], GPIO.IN)
        self.gpio.setup(pins["dac1_ld_bar"], GPIO.OUT)
        # pull down for transparent latches
        self.gpio.set_low(pins["dac1_ld_bar"])

        self.gpio.setup(pins["lat_dac2"], GPIO.OUT)
        self.gpio.set_low(pins["lat_dac2"])
        # low allows shift register GPIO.outputs to go to volatile memory

        self.gpio.setup(pins["rdy_adc1"], GPIO.IN)
        self.gpio.setup(pins["rdy_adc2"], GPIO.IN)
        self.gpio.setup(pins["rdy_adc3"], GPIO.IN)
        self.gpio.setup(pins["external_button"], GPIO.IN)

    def add_external_button_callback(self, cb):
    	self.gpio.add_event_detect(pins['external_button'], 1, callback=cb, bouncetime=10)

    def pulse_low(self, pin, sec):
        self.gpio.set_low(pin)
        time.sleep(sec)
        self.gpio.set_high(pin)

    def pulse_high(self, pin, sec):
        self.gpio.set_high(pin)
        time.sleep(sec)
        self.gpio.set_low(pin)

    def disable_eeprom(self):
        self.gpio.set_low(pins["eeprom_en"])

    # Only enable if VDD on PCBA is present
    def enable_eeprom(self):
        self.gpio.set_high(pins["eeprom_en"])

    def select_programmer_1(self):
        self.pulse_high(pins["sel_prog1"], 0.01)

    def select_programmer_2(self):
        self.pulse_high(pins["sel_prog2"], 0.01)

    def disable_backlight(self):
        self.gpio.set_high(pins["disable_backlight"])

    def enable_backlight(self):
        self.gpio.set_low(pins["disable_backlight"])

    def disable_vbat(self):
        self.gpio.set_high(pins["vbat_disable"])

    def enable_vbat(self):
        self.gpio.set_low(pins["vbat_disable"])

    def pushbutton_press(self, sec):
        self.gpio.set_high(pins["pushbutton"])
        time.sleep(sec)
        self.gpio.set_low(pins["pushbutton"])

    def push_short(self):
        pushbutton_press(0.05)

    def push_double(self):
        push_short()
        time.sleep(0.2)
        push_short()

    def push_long(self):
        pushbutton_press(1.0)

    def push_reset(self):
        pushbutton_press(9)
        time.sleep(1.0)
        pushbutton_press(0.05)

