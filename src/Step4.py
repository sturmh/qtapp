import sys
import signal
import time
from PyQt4 import QtGui
from PyQt4 import QtCore
from ui__step1 import Ui_step_1
from ButtonListWidgetItem import ButtonListWidgetItem
from threading import Thread
import logging
logger = logging.getLogger(__name__)

class Step4(QtGui.QWidget, Ui_step_1):
    buttons = ["IG", "AO"]

    waveform_test_complete_signal = QtCore.pyqtSignal(bool, list)

    def __init__(self, services, window, params = {}, parent = None):
        QtGui.QWidget.__init__(self, parent=parent)
        self.setupUi(self)
        self.window = window
        self.services = services
        self.serial0 = self.services['serial0']
        self.dac = self.services['dac']
        self.mcp4728 = self.services['mcp4728']
        self.mcp47feb22 = self.services['mcp47feb22']
        self.waveform_thread = None
        self.items = {}
        for indx, button in enumerate(self.buttons):
            itemWidget = ButtonListWidgetItem(button)
            itemWidget.setState(0)
            self.verticalLayout.addWidget(itemWidget);
            self.items[button] = itemWidget

        self.waveform_test_complete_signal.connect(self.__on_test_complete)

    def __on_test_complete(self, success, results):
        if success:
            self.waveform_thread.join()
            self.waveform_thread = None
            self.items['IG'].setState(2 if results[0] == True else 1)
            self.items['AO'].setState(2 if results[1] == True else 1)
            self.window.enter_pass_state("Press Yellow button to continue")
        else:
            self.window.enter_fail_state("FAILURE: Press Yellow button to restart")

    def __run_waveform_test(self):
        import dac_adc
        waveform_timestep_seconds = dac_adc.WAVEFORM_TIMESTEP
        waveform_voltages = dac_adc.WAVEFORM_VOLTAGES

        waveform_test_success = False
        self.waveform_test_running = True
        self.uart_stream = []
        
        logger.debug("flushing uart before starting test")
        self.serial0.flush()
        logger.debug("flushed uart output:{0}".format(self.serial0.read(self.serial0.inWaiting())))
        uart_thread = Thread(target=self.__monitor_uart, args=())
        uart_thread.start()

        init_values = None
        start = time.time()
        while time.time() - start < 2:
            if len(self.uart_stream) > 0:
                init_values = self.uart_stream[-1]
                break
            logger.debug("Waiting for initial serial output")
            time.sleep(0.1)

        if init_values == None:
            logger.error("Failed to get initialization values from uart. No uart output so failing test")
            waveform_test_success = False
        else:
            logger.debug("do_waveform_init with {0}".format(init_values))
            dac_adc.do_waveform_init(init_values[1], init_values[2])
        
            waveform_test_start_timestamp = time.time()
            for voltage in waveform_voltages:
                logger.debug("waveform step with volage:{0}".format(voltage))
                dac_adc.do_waveform_step(self.mcp4728, self.mcp47feb22, voltage)
                time.sleep(waveform_timestep_seconds)
            
            dac_adc.do_waveform_on_complete()
            waveform_test_success = True

        self.waveform_test_running = False
        #intentionally not joining thread so we dont have to wait for it 
        #uart_thread.join()
        test_results = []

        if waveform_test_success:
            c1_min = sys.maxint
            c2_min = sys.maxint
            c1_max = -sys.maxint
            c2_max = -sys.maxint
            start_idx = 0
            for i in self.uart_stream:
                ts = i[0]
                c1 = i[1]
                c2 = i[2]
                if ts >= waveform_test_start_timestamp + 0.1:
                    c1_min = min(c1_min, c1)
                    c2_min = min(c2_min, c2)
                    c1_max = max(c1_max, c1)
                    c2_max = max(c2_max, c2)
            
            logger.debug("determine_waveform_test_result with c1_min:{0}, c1_max:{1}, c2_min:{2}, c2_max:{3}".format(c1_min, c1_max, c2_min, c2_max))
            test_results = dac_adc.determine_waveform_test_result(c1_min, c1_max, c2_min, c2_max)
            logger.debug("Waveform test results:{0}".format(test_results))

        self.uart_stream = []
        self.waveform_test_complete_signal.emit(waveform_test_success, test_results)

    def __monitor_uart(self):
        while self.waveform_test_running != False:
            line = self.serial0.readline()
            logger.debug("uart output: {0}".format(line.rstrip()))
            s = line.split(",")
            if len(s) != 2:
                logger.debug("unexpected serial output:{0}".format(line))
                continue

            ## hobo ring buffer
            if len(self.uart_stream) > 5000:
                self.uart_stream.pop(0)
            try:
                self.uart_stream.append((time.time(), float(s[0]), float(s[1])))
            except ValueError:
                pass
        logger.debug("__monitor_uart exiting")

    # called whenever this widget is transitioning to being the current widget
    def on_enter(self, state):
        logger.debug("entering")
        if self.serial0 == None:
            logger.warning("No serial detected for waveform test")
            self.window.enter_fail_state("No serial connected")
            return

        self.reset()

        self.window.start_loading_bar("Testing")
        self.waveform_thread = Thread(target=self.__run_waveform_test, args=())
        self.waveform_thread.start()

    # called whenever this widget is transitioning from being the current widget
    def on_exit(self, state):
        logger.debug("exiting")
        self.reset()

    # called whenever this widget is maintain focus as current widget but needs to reset state
    def reset(self):
        for key, button in self.items.iteritems():
            button.setState(0)

        self.uart_stream = []
        if self.waveform_thread != None:
            self.waveform_thread.join()
            self.waveform_thread = None

        self.window.set_status_bar("Press Yellow button to continue")

    def run_step(self):
        logger.debug("run step - do nothing")
        pass

