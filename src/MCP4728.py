#!/usr/bin/python


class MCP4728:
    i2c = None
        
    # Registers
    __REG_WRITEALLDAC   = 0x50
    __REG_WRITEPROMCH0  = 0x58
    __REG_WRITEPROMCH1  = 0x5A
    __REG_WRITEPROMCH2  = 0x5C
    __REG_WRITEPROMCH3  = 0x5E

    # Constructor
    def __init__(self, address=0x60, busnum=1, debug=False):
        from Adafruit_GPIO.I2C import Device as Adafruit_I2C
        self.i2c = Adafruit_I2C(address, busnum)
        self.address = address
        self.debug = debug
                 

    def __updatebyte(self, byte, mask, value):
        byte &= mask
        byte |= value
        return byte

    def single_internal(self, channel, volt):
        if(volt>2.0): gain=2
        else: gain=1
        value = int(4096 * volt/2.048/gain + 0.5)
        print("{} {} {}".format(gain, volt, value))
        if (value>4095): value=4095
        if (value<0):    value=0
        self.single_raw(channel, 1, gain-1, value)

    def single_external(self, channel, rel):
        value = int(4096 * rel + 0.5)
        if (value>4095): value=4095
        if (value<0):    value=0
        self.single_raw(channel, 0, 0, value)

    # gain=1, VDD reference
    def set_value_vddref(self, channel, raw):
        self.single_raw(channel, 1, 0, raw)

    # gain=1, 2.048V reference
    def set_value(self, channel, raw):
        self.single_raw(channel, 0, 0, raw)

    def set_voltage(self, channel, volt):
        self.single_internal(channel, volt)
        #self.single_external(channel, rel*3.3)

    def single_raw(self, channel, reference, gain, value):
        """
        writes single raw value to the selected DAC channel - channels 1 to 4
        """
        second, third = divmod(value, 0x100)
        first=self.__updatebyte(0x58,0xFF,(channel-1) << 1)
        second=self.__updatebyte(second,0x0F,reference << 7 | gain << 4)
        self.i2c.writeList(first,[second, third])        


    def set_all_voltage(self, v0, v1, v2, v3):
        voltages = [min(max(0, v), 4095) for v in [v0, v1, v2, v3]]
        upper_bytes = [(v >> 8) & 0xFF for v in voltages]
        lower_bytes = [v & 0xFF for v in voltages]
        bytes = [x for y in zip(upper_bytes, lower_bytes) for x in y]
        print bytes
        self.i2c.writeList(0x50, bytes)

