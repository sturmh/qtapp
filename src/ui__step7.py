# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'step7.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_step_7(object):
    def setupUi(self, step_7):
        step_7.setObjectName(_fromUtf8("step_7"))
        step_7.resize(400, 300)

        self.retranslateUi(step_7)
        QtCore.QMetaObject.connectSlotsByName(step_7)

    def retranslateUi(self, step_7):
        step_7.setWindowTitle(_translate("step_7", "Form", None))

