# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'step1.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_step_1(object):
    def setupUi(self, step_1):
        step_1.setObjectName(_fromUtf8("step_1"))
        step_1.resize(400, 300)
        self.verticalLayout = QtGui.QVBoxLayout(step_1)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))

        self.retranslateUi(step_1)
        QtCore.QMetaObject.connectSlotsByName(step_1)

    def retranslateUi(self, step_1):
        step_1.setWindowTitle(_translate("step_1", "Form", None))

