import sys
import signal
from PyQt4 import QtGui
from PyQt4 import QtCore
from window import Window
import logging
from logging.config import dictConfig
from dac import DAC
import serial
from gpio import GPIO
from MCP4728 import MCP4728
from MCP47FEB22 import MCP47FEB22
import argparse
logger = logging.getLogger(__name__)


if __name__ == '__main__':
    logging_config = dict(
        disable_existing_loggers=False,
        version = 1,
        formatters = {
            'f': {'format':
                  '%(asctime)s %(name)-12s %(threadName)s %(levelname)-8s %(message)s'}
            },
        handlers = {
            'h': {'class': 'logging.StreamHandler',
                  'formatter': 'f',
                  'level': logging.DEBUG
                  },
            'fh': {
                    'class': 'logging.handlers.RotatingFileHandler',
                    'level': logging.DEBUG,
                    'formatter': 'f',
                    'filename': '/home/pi/qtapp.log',
                    'mode': 'a',
                    'maxBytes': 10485760,
                    'backupCount': 5,
                  }
            },

        root = {
            'handlers': ['h', 'fh'],
            'level': logging.DEBUG,
            },
    )
    dictConfig(logging_config)

    logger.debug("######################################################################################################")
    logger.debug("######################################################################################################")
    logger.debug("######################################################################################################")

    parser = argparse.ArgumentParser(description="")
    parser.add_argument('--adc3-is-16bit', help='Switches from using 12bit adc3 to 16bit adc3', default=False, action='store_true')
    parser.add_argument('--use-touchscreen', help='Switches from physical button to Touchscreen', default=False, action='store_true')
    args = parser.parse_args()
    logger.debug("argparse results:{0}".format(args))

    services = {'dac':None, 'serial0':None, 'camera':None, 'gpio':None, 'mcp4728':None, 'mcp47feb22':None}
    try:
        services['mcp47feb22'] = MCP47FEB22()
    except:
        logger.exception("Failed to initialize MCP47FEB22")
    try:
        services['mcp4728'] = MCP4728()
    except:
        logger.exception("Failed to initialize MCP4728")
    try:
        services['dac'] = DAC(args.adc3_is_16bit)
    except:
        logger.exception("Failed to initialize DAC")
    try:
        services['serial0'] = serial.Serial(port='/dev/serial0', baudrate=57600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=5)
    except:
        logger.exception("Failed to initialize serial")
    try:
        from picamera import PiCamera
        services['camera'] = PiCamera()
        services['camera'].resolution = (1920, 1080)
    except:
        logger.exception("Failed to initialize camera")
    try:
        services['gpio'] = GPIO()
    except:
        logger.exception("Failed to initialize GPIO")


    signal.signal(signal.SIGINT, signal.SIG_DFL) # makes ctrl-c work
    app = QtGui.QApplication(sys.argv)
    w = Window(services, args.use_touchscreen)
    w.showFullScreen()
    sys.exit(app.exec_())
