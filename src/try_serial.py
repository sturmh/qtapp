import serial
import time

ser = serial.Serial(port='/dev/serial0', baudrate=57600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=30)

import dac_adc

dac_adc.disable_vbat()
time.sleep(1)
dac_adc.enable_vbat()

while True:
    received_data = ser.read()              #read serial port
    time.sleep(0.03)    
    data_left = ser.inWaiting()             #check for remaining byte    
    received_data += ser.read(data_left)    
    print (received_data)                   #print received data
