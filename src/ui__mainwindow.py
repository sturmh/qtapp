# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_main_window(object):
    def setupUi(self, main_window):
        main_window.setObjectName(_fromUtf8("main_window"))
        main_window.resize(480, 800)
        main_window.setMinimumSize(QtCore.QSize(480, 800))
        main_window.setMaximumSize(QtCore.QSize(480, 800))
        self.centralwidget = QtGui.QWidget(main_window)
        self.centralwidget.setMaximumSize(QtCore.QSize(480, 800))
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.central_layout = QtGui.QVBoxLayout(self.centralwidget)
        self.central_layout.setObjectName(_fromUtf8("central_layout"))
        self.top_widget = QtGui.QWidget(self.centralwidget)
        self.top_widget.setObjectName(_fromUtf8("top_widget"))
        self.top = QtGui.QGridLayout(self.top_widget)
        self.top.setMargin(0)
        self.top.setObjectName(_fromUtf8("top"))
        self.central_layout.addWidget(self.top_widget)
        self.bottom_widget = QtGui.QWidget(self.centralwidget)
        self.bottom_widget.setMinimumSize(QtCore.QSize(0, 150))
        self.bottom_widget.setMaximumSize(QtCore.QSize(16777215, 150))
        self.bottom_widget.setObjectName(_fromUtf8("bottom_widget"))
        self.bottom = QtGui.QGridLayout(self.bottom_widget)
        self.bottom.setMargin(0)
        self.bottom.setObjectName(_fromUtf8("bottom"))
        self.central_layout.addWidget(self.bottom_widget)
        main_window.setCentralWidget(self.centralwidget)

        self.retranslateUi(main_window)
        QtCore.QMetaObject.connectSlotsByName(main_window)

    def retranslateUi(self, main_window):
        main_window.setWindowTitle(_translate("main_window", "MainWindow", None))

