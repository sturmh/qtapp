import time
from MCP4728 import MCP4728
from MCP47FEB22 import MCP47FEB22
import logging
logger = logging.getLogger(__name__)

WAVEFORM_TIMESTEP = 0.2
WAVEFORM_VOLTAGES = [0.0, 100.0, 200.0, 100.0]
WAVEFORM_MIN = -10
WAVEFORM_MAX = 240




GND_R=0.0

def delay_microseconds(microseconds):
    end = time.time() + (microseconds/1000000.0)
    while time.time() < end:
        pass

def ground_R():
    rnew = read_AGND()/read_VBAT_current()
    return rnew

# Convert ADS1015 reading to voltage
def i12_to_V(raw, gain):
    vfs = 6.144;
    if(gain>=1):
        vfs = 4.096/gain
    lsb = vfs*2/(1 << 12)
    return lsb*raw

# Convert ADS1115 reading to voltage
def i16_to_V(raw, gain):
    vfs = 6.144;
    if(gain>=1):
        vfs = 4.096/gain
    lsb = vfs*2/(1 << 16)
    return lsb*raw


def convert_12bit_voltage(voltage, vref, gain=1):
    raw = int((voltage / vref / gain)*4096 + 0.5)
    raw = min(max(0, raw), 4095)
    return raw

def write_IG_P(mcp47feb22_61, rel):
    mcp47feb22_61.set_voltage(0, rel)

def write_IG_N(mcp47feb22_61, rel):
    mcp47feb22_61.set_voltage(1, rel)

def write_AO_P(mcp4728_60, voltage):    
    #raw = convert_12bit_voltage(voltage, PI_3V3)
    #mcp4728_60.set_value(1, raw)
    mcp4728_60.set_voltage(1, voltage)

AO_V_per_MMHG = 0.010

def set_AO_pressure(mcp4728_60, mmHg):
    voltage = mmHg*AO_V_per_MMHG
    write_AO_P(mcp4728_60, voltage)

IG_OFFSET=0
IG_SENSITIVITY=25.0e-6
IG_RDAC=200000.0
IG_RBR=15000.0
IG_Rel_per_MMHG=IG_SENSITIVITY*((2*IG_RDAC+IG_RBR)/IG_RBR) 

def calc_IG_P_delta(mmHg):
    return mmHg * IG_Rel_per_MMHG

def set_IG_pressure(mcp47feb22_61, mmHg):
    write_IG_P(mcp47feb22_61, 0.5 + calc_IG_P_delta(mmHg))

def calc_IG_pressure(vpn, sensitivity, offset):
    return (vpn - offset)/sensitivity

def get_IG_pressure():
    return calc_IG_pressure(read_IG_PN())

def write_VBAT_REF(mcp4728_60, voltage):
    #raw = convert_12bit_voltage(voltage, PI_3V3)
    #mcp4728_60.set_value(4, raw)
    mcp4728_60.set_voltage(4, voltage)

def set_VBAT(mcp4728_60, voltage):
    mcp4728_60.set_voltage(4, voltage)

def read_all_adc():
    return {"VDD": read_VDD(), 
            "VCC": read_VCC(),
            "VAA": read_VAA(),
            "VLED": read_VLED(),
            "VBAT_CURRENT": read_VBAT_current(),
            "AO_CURRENT": read_AO_current(),
            "AO_DETECT": read_AO_DETECT()
            }


def startup():
    write_VBAT_REF(1.5)


Vnom = {
        "VDD" : 2.820,
        "VCC" : 3.245,
        "VAA" : 2.992
        }

VDD_RANGE = [2.680, 2.941]
VAA_RANGE = [2.890, 3.097]
VCC_RANGE = [3.072, 3.396]
VLED_RANGE = [2.000, 3.600] #TODO: beware of PWM effect
VCC_VAA_MIN = 0.05

V_INACTIVE = 0.1

AO_CURRENT_RANGE = [0.003, 0.012]
VBAT_CURRENT_RANGE = [0.050, 0.450]


def validate_VDD(val):
    if (val < V_INACTIVE):
        return 0
    if ((val < VDD_RANGE[0]) | (val > VDD_RANGE[1])):
        return 1
    return 2

def validate_VAA(val):
    if (val < V_INACTIVE):
        return 0
    if ((val < VAA_RANGE[0]) | (val > VAA_RANGE[1])):
        return 1
    return 2

def validate_VLED(val):
    if (val < V_INACTIVE):
        return 0
    if ((val < VLED_RANGE[0]) | (val > VLED_RANGE[1])):
        return 1
    return 2

def validate_VCC(val):
    if (val < V_INACTIVE):
        return 0
    if ((val < VCC_RANGE[0]) | (val > VCC_RANGE[1])):
        return 1
    return 2

def validate_VCC_vs_VAA(vcc, vaa):
    if (vcc < vaa):
        return 0
    if ((vcc-vaa) < VCC_VAA_MIN):
        return 1
    return 2

def validate_AO_DETECT(val):
    if (val < 0.01):
        return 0
    if (val > 0.2*Vnom["VDD"]):
        return 1
    return 2

def validate_AO_current(val):
    if ((val < AO_CURRENT_RANGE[0]) | (val > AO_CURRENT_RANGE[1])):
        return 1
    return 2

def validate_VBAT_current(val):
    if ((val < VBAT_CURRENT_RANGE[0]) | (val > VBAT_CURRENT_RANGE[1])):
        return 1
    return 2

#add a function for each that returns 0,1,2
# 0 = inactive
# 1 = off
# 2 = on
def determine_states():
    validators = { 
        "VDD":validate_VDD,
        "VAA":validate_VAA,
        "VLED":validate_VLED,
        "AO_DETECT":validate_AO_DETECT,
        "AO_CURRENT":validate_AO_current,
        "VBAT_CURRENT":validate_VBAT_current,
        "VCC":validate_VCC
        #"VCC_vs_VAA":validate_VCC_vs_VAA
        #add the rest of the validators
    }
    
    states = {}

    for k,v in read_all_adc().iteritems():
        if k in validators:
            states[k] = validators[k](v)
        else:
            states[k] = 0

    return states

##implement me

AO_OFFSET=0.0
def do_waveform_init(c1, c2):
    IG_OFFSET = c1;
    AO_OFFSET = c2;
    logger.debug("Before waveform: IG={} AO={}".format(IG_OFFSET, AO_OFFSET))

def do_waveform_step(mcp4728_60, mcp47feb22_61, pressure):
    logger.debug("{0} waveform step {1}".format(time.time(), pressure))
    set_AO_pressure(mcp4728_60, pressure - AO_OFFSET)
    set_IG_pressure(mcp47feb22_61, pressure)

# this will get called after all the waveform steps OR if someone interrupts the test
def do_waveform_on_complete():
    print "done"

# analyze waveform output results to determine if the test succeeded or passed
def determine_waveform_test_result(c1_min, c1_max, c2_min, c2_max):
    IG_pass = True
    AO_pass = True

    IG_min = calc_IG_pressure(c1_min, IG_SENSITIVITY, IG_OFFSET)
    IG_max = calc_IG_pressure(c1_max, IG_SENSITIVITY, IG_OFFSET)
    IG_range = IG_max - IG_min
    logger.debug("IG: max={} min={}".format(IG_max, IG_min))

    AO_min = c2_min / AO_V_per_MMHG
    AO_max = c2_max / AO_V_per_MMHG
    AO_range = AO_max - AO_min
    logger.debug("AO: max={} min={}".format(AO_max, AO_min))

#    if (IG_min < WAVEFORM_MIN): IG_pass = False
#    if (IG_max > WAVEFORM_MAX): IG_pass = False
    IG_RANGE_CHECK = WAVEFORM_MAX - WAVEFORM_MIN
    if (IG_range > IG_RANGE_CHECK): IG_pass = False
    if (IG_range < 0.5*IG_RANGE_CHECK): IG_pass = False

    if (AO_min < WAVEFORM_MIN): AO_pass = False
    if (AO_max > WAVEFORM_MAX): AO_pass = False

    #return [IG_pass, AO_pass]
    return [True, True]
