import sys
import signal
from PyQt4 import QtGui
from PyQt4 import QtCore
from ui__step2 import Ui_step_2
import logging
logger = logging.getLogger(__name__)
import os
import time

def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS,
        # and places our data files in a folder relative to that temp
        # folder named as specified in the datas tuple in the spec file
        base_path = os.path.join(sys._MEIPASS, 'data')
    except Exception:
        # sys._MEIPASS is not defined, so use the original path
        base_path = '../data/'

    return os.path.join(base_path, relative_path)

class Step2(QtGui.QWidget, Ui_step_2):

    def __init__(self, services, window, params = {}, parent = None):
        QtGui.QWidget.__init__(self, parent=parent)
        self.setupUi(self)
        self.window = window
        self.services = services
        self.gpio = self.services['gpio']

        self.p1_gif = QtGui.QMovie(resource_path('p1.gif'))
        self.p1_gif.setScaledSize(self.center_image.size())
        self.center_image.setMovie(self.p1_gif)
        self.description.setText("Press the black button on P1 to program and wait for the PCBA to reset before continuing")
        self.description.setWordWrap(True)
        font = QtGui.QFont()
        font.setPointSize(18)
        self.description.setFont(font)

    # called whenever this widget is transitioning to being the current widget
    def on_enter(self, state):
        logger.debug("entering")
        self.p1_gif.start()
        if self.gpio == None:
            self.window.enter_fail_state("GPIO not connected")
            return
        else:
            self.gpio.select_programmer_1()
            self.window.set_status_bar("Press Yellow button after programming to continue")


    # called whenever this widget is transitioning from being the current widget
    def on_exit(self, state):
        self.p1_gif.stop()
        logger.debug("exiting")
        self.reset()

    # called whenever this widget is maintain focus as current widget but needs to reset state
    def reset(self):
        pass

    def run_step(self):
        if self.gpio == None:
            self.window.reset()
        else:
            self.window.advance()
