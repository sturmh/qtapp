import time
import sys
import signal
import logging
from logging.config import dictConfig
from dac import DAC
import serial
from gpio import GPIO
from MCP4728 import MCP4728
from MCP47FEB22 import MCP47FEB22
import math

adc3_is_16bit = True

logger = logging.getLogger(__name__)

logging_config = dict(
    disable_existing_loggers=False,
    version = 1,
    formatters = {
        'f': {'format':
              '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'}
#              '%(asctime)s %(name)-12s %(threadName)s %(levelname)-8s %(message)s'}
        },
    handlers = {
        'h': {'class': 'logging.StreamHandler',
              'formatter': 'f',
              'level': logging.DEBUG
              },
        'fh': {
                'class': 'logging.handlers.RotatingFileHandler',
                'level': logging.DEBUG,
                'formatter': 'f',
                'filename': '/home/pi/rpcb.log',
                'mode': 'a',
                'maxBytes': 10485760,
                'backupCount': 5,
              }
        },

    root = {
        'handlers': ['h', 'fh'],
        'level': logging.DEBUG,
        },
)
dictConfig(logging_config)

logger.debug("######################################################################################################")
logger.debug("######################################################################################################")
logger.debug("######################################################################################################")

services = {'dac':None, 'serial0':None, 'gpio':None, 'mcp4728':None, 'mcp47feb22':None}
try:
    services['mcp47feb22'] = MCP47FEB22()
except:
    logger.exception("Failed to initialize MCP47FEB22")
try:
    services['mcp4728'] = MCP4728()
except:
    logger.exception("Failed to initialize MCP4728")
try:
    services['dac'] = DAC(adc3_is_16bit)
except:
    logger.exception("Failed to initialize DAC")
try:
    services['serial0'] = serial.Serial(port='/dev/serial0', baudrate=57600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=5)
except:
    logger.exception("Failed to initialize serial")
try:
    services['gpio'] = GPIO()
except:
    logger.exception("Failed to initialize GPIO")

signal.signal(signal.SIGINT, signal.SIG_DFL) # makes ctrl-c work

gpio = services['gpio']
dac = services['dac']

gpio.disable_backlight()    # to reduce cable IR drop, noise

def avgRead(n, t, fn):
    a=[]
    for i in range(n):
        time.sleep(t)
        a.append(fn())
    s=0
    for i in a:
        s = s+i
    s=s/n
    return s
#    v=0
#    for i in a:
#        v=v+(i-s)*(i-s)
#    v=v/n
#    return [s, math.sqrt(v)]

def calc_Rbr_Rpr(Vt, err):
    # Rbr_R/Rpcb_R = (1-2*(Vtr+err))/(1+2*(Vt+err))
    # where
    #  Vt is differential voltage from IG_N to a dummy didiver
    #  err is the deviation of the dummy divider from 1/2
    return (1-2*(Vt+err))/(1+2*(Vt+err))

def calc_Rbl_Rpl(Vp, Rbr_Rpr):
    # Rbr_L/Rpcb_L = ((Rbr_R/Rpcb_R) - Vp*(1+Rbr_R/Rpcb_R)) /
    #                 (1 + Vp*(1+Rbr_R/Rpcb_R))
    #              = (1-2*(Vp+Vt+err))/(1+2*(Vp+Vt+err))
    return (Rbr_Rpr - Vp*(1+Rbr_Rpr)) / (1 + Vp*(1+Rbr_Rpr))



n=10
t=0.05

agnd = avgRead(n, t, dac.read_AGND) 
vaa = avgRead(n, t, dac.read_VAA)
vmid = (0.5*(vaa-agnd)+agnd)/vaa
#vmid = 0.5
services['mcp47feb22'].set_voltage(0, vmid)
services['mcp47feb22'].set_voltage(0, vmid)

igP = avgRead(n, t, dac.read_IG_PN)
igT = avgRead(n, t, dac.read_IG_T)
igD = avgRead(n, t, dac.read_IG_D)


igPr = igP/(vaa-agnd)
igTr = igT/(vaa-agnd)
igDr = (igD-agnd)/(vaa-agnd)
err = igDr-0.5


print vaa, agnd, vmid
print igPr, igTr
print igDr, err
Rbr_Rpr = calc_Rbr_Rpr(igTr, err)
Rbl_Rpl = calc_Rbl_Rpl(igPr, Rbr_Rpr)
print("Rpcb_L {}\nRpcb_R {}".format(15000/Rbl_Rpl, 15000/Rbr_Rpr))
#print



gpio.enable_backlight() # restore desired state

