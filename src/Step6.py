import sys
import signal
from PyQt4 import QtGui
from PyQt4 import QtCore
from ui__step3 import Ui_step_3
import logging
import os
logger = logging.getLogger(__name__)

def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS,
        # and places our data files in a folder relative to that temp
        # folder named as specified in the datas tuple in the spec file
        base_path = os.path.join(sys._MEIPASS, 'data')
    except Exception:
        # sys._MEIPASS is not defined, so use the original path
        base_path = '../data/'

    return os.path.join(base_path, relative_path)

class Step6(QtGui.QWidget, Ui_step_3):
    image_name = resource_path('verify_pcba_2')

    def __init__(self, services, window, params = {}, parent = None):
        QtGui.QWidget.__init__(self, parent=parent)
        self.setupUi(self)
        self.window = window
        self.services = services

        image = QtGui.QPixmap(self.image_name)
        if image.isNull():
            logger.warning("Failed to load image: {0}", self.image_name)

        scaled_pixmap = image.scaled(self.image.size(), QtCore.Qt.KeepAspectRatio)
        self.image.setPixmap(scaled_pixmap)
        self.description.setText("Verify PCBA Screen")
        self.description.setWordWrap(True)
        font = QtGui.QFont()
        font.setPointSize(18)
        self.description.setFont(font)

    # called whenever this widget is transitioning to being the current widget
    def on_enter(self, state):
        logger.debug("entering")
        self.reset()
        self.window.enter_pass_state("Press Yellow button to after verifying to continue")

    # called whenever this widget is transitioning from being the current widget
    def on_exit(self, state):
        logger.debug("exiting")
        self.reset()

    # called whenever this widget is maintain focus as current widget but needs to reset state
    def reset(self):
        pass

    def run_step(self):
        logger.debug("run")