# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'button_listwidget_item.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_button_listwidget_item(object):
    def setupUi(self, button_listwidget_item):
        button_listwidget_item.setObjectName(_fromUtf8("button_listwidget_item"))
        button_listwidget_item.setEnabled(True)
        button_listwidget_item.resize(480, 55)
        button_listwidget_item.setMinimumSize(QtCore.QSize(0, 55))
        button_listwidget_item.setMaximumSize(QtCore.QSize(480, 55))
        self.horizontalLayout = QtGui.QHBoxLayout(button_listwidget_item)
        self.horizontalLayout.setMargin(5)
        self.horizontalLayout.setSpacing(5)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.status_led = QtGui.QLabel(button_listwidget_item)
        self.status_led.setMinimumSize(QtCore.QSize(45, 45))
        self.status_led.setMaximumSize(QtCore.QSize(45, 45))
        self.status_led.setText(_fromUtf8(""))
        self.status_led.setObjectName(_fromUtf8("status_led"))
        self.horizontalLayout.addWidget(self.status_led, 0, QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
        self.label = QtGui.QLabel(button_listwidget_item)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label, 0, QtCore.Qt.AlignLeft)

        self.retranslateUi(button_listwidget_item)
        QtCore.QMetaObject.connectSlotsByName(button_listwidget_item)

    def retranslateUi(self, button_listwidget_item):
        button_listwidget_item.setWindowTitle(_translate("button_listwidget_item", "Form", None))
        self.label.setText(_translate("button_listwidget_item", "TextLabel", None))

