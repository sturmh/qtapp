#/bin/sh

cd src/ui
for f in *
do
    OUTPUT="$(echo ${f} | sed 's/.ui//')"
    pyuic4 $f > ../ui__$OUTPUT.py
done
