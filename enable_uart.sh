#!/bin/bash

sudo grep -q "dtoverlay*" /boot/config.txt || echo "dtoverlay=pi3-disable-bt" >> /boot/config.txt
sudo sed -i "/dtoverlay/c dtoverlay=pi3-disable-bt" /boot/config.txt 