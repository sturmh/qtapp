#!/bin/sh

sudo sed -i '/^display_rotate=/{h;s/=.*/=0/};${x;/^$/{s//display_rotate=0/;H};x}' /boot/config.txt
grep -q DISPLAY=:0* ~/.bashrc || echo DISPLAY=:0 xinput --set-prop FT5406 memory based driver Coordinate Transformation Matrix 1 0 0 0 1 0 0 0 1 >> ~/.bashrc
sed -i  "/DISPLAY=:0/c DISPLAY=:0 xinput --set-prop 'FT5406 memory based driver' 'Coordinate Transformation Matrix'  1 0 0 0 1 0 0 0 1" ~/.bashrc

echo 'reboot to take effect'

