#!/bin/sh

sudo sed -i '/^display_rotate=/{h;s/=.*/=1/};${x;/^$/{s//display_rotate=1/;H};x}' /boot/config.txt

echo 'reboot to take effect'
grep -q  DISPLAY=:0* ~/.bashrc || echo DISPLAY=:0 xinput --set-prop FT5406 memory based driver Coordinate Transformation Matrix 0 1 0 -1 0 1 0 0 1 >> ~/.bashrc
sed -i "/DISPLAY=:0/c DISPLAY=:0 xinput --set-prop 'FT5406 memory based driver' 'Coordinate Transformation Matrix'  0 1 0 -1 0 1 0 0 1" ~/.bashrc
